import {Ionicons} from '@expo/vector-icons';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import * as React from 'react';
import {AsyncStorage} from 'react-native';

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import SearchScreen from '../screens/SearchScreen';
import AccountScreen from '../screens/AccountScreen';
import {BottomTabParamList, SearchParamList, AccountParamList} from '../types';
import LoginScreen from "../screens/LoginScreen";

const BottomTab = createBottomTabNavigator<BottomTabParamList>();


export default function BottomTabNavigator() {

    const [token, setToken] = React.useState({})

    const getToken = async () => {
        setToken(await AsyncStorage.getItem('token'));
    }
    getToken()
    const colorScheme = useColorScheme();
    return (
        <BottomTab.Navigator
            initialRouteName="Search"
            tabBarOptions={{activeTintColor: Colors[colorScheme].tint}}>
            <BottomTab.Screen
                name="Rechercher"
                component={SearchNavigator}
                options={{
                    tabBarIcon: ({color}) => <TabBarIcon name="ios-code" color={color}/>,
                }}
            />
            <BottomTab.Screen
                name="Compte"
                component={token !== null ? AccountNavigator : LoginScreen}
                options={{
                    tabBarIcon: ({color}) => <TabBarIcon name="ios-code" color={color}/>,
                }}
            />
        </BottomTab.Navigator>
    );
}


// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
function TabBarIcon(props: { name: string; color: string }) {
    return <Ionicons size={30} style={{marginBottom: -3}} {...props} />;
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const SearchStack = createStackNavigator<SearchParamList>();

function SearchNavigator() {
    return (
        <SearchStack.Navigator>
            <SearchStack.Screen
                name="SearchScreen"
                component={SearchScreen}
                options={{headerTitle: 'LeBonCoin'}}
            />
        </SearchStack.Navigator>
    );
}

const AccountStack = createStackNavigator<AccountParamList>();

function AccountNavigator() {
    return (
        <AccountStack.Navigator>
            <AccountStack.Screen
                name="AccountScreen"
                component={AccountScreen}
                options={{headerTitle: 'LeBonCoin'}}
            />
        </AccountStack.Navigator>
    );
}
