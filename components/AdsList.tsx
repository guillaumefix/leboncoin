import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import {StyleSheet, TouchableOpacity, AsyncStorage} from 'react-native';
import {Text, View} from '../components/Themed';
import {Table, TableWrapper, Row, Rows, Col, Cols, Cell} from 'react-native-table-component';

export default function AdsList({path}: { path: string }) {
    const [token, setToken] = React.useState({})
    const [isReady, setIsReady] = React.useState(false)
    const [tableHead, setTableHead] = React.useState(null)
    const [tableData, setTableData] = React.useState(null)
    const getToken = async () => {
        setToken(await AsyncStorage.getItem('token'));
    }
    React.useEffect(() => {
        if (tableData === null)
            getToken()
                .then(() => {
                    fetch('http://46.101.147.201:1337/ads', {
                        method: 'GET',
                        headers: {
                            'Authorization': 'Bearer ' + token,
                            Accept: 'application/json',
                            'Content-Type': 'application/json'
                        },
                    }).then((response) => response.json())
                        .then((json) => {
                            let head = [];
                            let data = [];
                            head.push(
                                'id', 'title', 'description'
                            );
                            for (let i = 0; i < json.length; i++) {
                                data.push([
                                        json[i].id,
                                        json[i].title,
                                        json[i].description,
                                    ]
                                )
                            }
                            setTableData(data);
                            setTableHead(head);
                            setIsReady(true)
                            console.log(tableData)
                            console.log(data)
                        })
                        .catch((error) => {
                            console.error(error);
                        });
                })
    });

    return (
        <View style={styles.container}>
            <View style={styles.getStartedContainer}>
                <Text style={styles.title}>
                    Annonces fraîchement publiées
                </Text>
                <View isReady={isReady}>
                    <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
                        <Row data={tableHead} style={styles.head} textStyle={styles.text}/>
                        <Rows data={tableData} textStyle={styles.text}/>
                    </Table>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 0.6,
        backgroundColor: '#fff',
    },
    developmentModeText: {
        marginBottom: 20,
        fontSize: 14,
        lineHeight: 19,
        textAlign: 'center',
    },
    contentContainer: {
        paddingTop: 30,
    },
    welcomeContainer: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20,
    },
    welcomeImage: {
        width: 100,
        height: 80,
        resizeMode: 'contain',
        marginTop: 3,
        marginLeft: -10,
    },
    getStartedContainer: {},
    homeScreenFilename: {
        marginVertical: 7,
    },
    codeHighlightText: {
        color: 'rgba(96,100,109, 0.8)',
    },
    codeHighlightContainer: {
        borderRadius: 3,
        paddingHorizontal: 4,
    },
    getStartedText: {
        fontSize: 17,
        lineHeight: 24,
    },
    helpContainer: {
        marginTop: 15,
        marginHorizontal: 20,
    },
    helpLink: {
        paddingVertical: 15,
    },
    title: {
        fontWeight: 'bold',
    },
    head: {height: 40, backgroundColor: '#f1f8ff'},
    text: {margin: 6}
});
