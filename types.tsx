export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  TabOne: undefined;
  Account: undefined;
};

export type TabOneParamList = {
  TabOneScreen: undefined;
};

export type AccountParamList = {
  AccountScreen: undefined;
};
