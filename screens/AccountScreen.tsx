import * as React from 'react';
import {StyleSheet, TextInput, TouchableOpacity} from 'react-native';
import {AsyncStorage} from 'react-native';
import {Text, View} from '../components/Themed';
import jwt_decode from 'jwt-decode';

export default function AccountScreen({navigation: {navigate}}) {
    const [name, setName] = React.useState({})
    const [email, setEmail] = React.useState({})
    const [password, setPassword] = React.useState({})
    const [token, setToken] = React.useState({})
    const [userInfo, setUserInfo] = React.useState({name: null, email: null})
    const getToken = async () => {
        const promise = new Promise(async (resolve, reject) => {
            setToken(await AsyncStorage.getItem('token'));
            resolve();
        });
        return promise;
    }
    React.useEffect(() => {
        if (userInfo.email === null)
            getToken()
                .then(() => {
                    setUserInfo(jwt_decode(token.toString()));
                    if (userInfo == null) {
                        navigate('Rechercher');
                    }
                })
    });

    console.log(userInfo)

    const handleSubmit = (e) => {

        fetch('http://46.101.147.201:1337/users/' + userInfo.id, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({email: email, password: password, name: name})
        }).then((response) => response.json())
            .then((json) => {
                console.error(json);
            })
            .catch((error) => {
                console.error(error);
            });
    }
    const logout = async () => {
        try {
            await AsyncStorage.removeItem('token');
            navigate('Rechercher');
        } catch (error) {
            // Error saving data
        }
    };

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Compte</Text>
            <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)"/>
            <Text style={styles.welcome}>Bonjour {userInfo.name ? userInfo.name : userInfo.email}!</Text>
            <Text style={styles.info}>Bienvenue sur votre compte.</Text>
            <View style={styles.inputView}>
                <TextInput
                    style={styles.inputText}
                    placeholder="Nom..."
                    placeholderTextColor="#003f5c"
                    value={userInfo.name}
                    onChangeText={value => setName(value)}/>
            </View>
            <View style={styles.inputView}>
                <TextInput
                    style={styles.inputText}
                    placeholder="Email..."
                    placeholderTextColor="#003f5c"
                    value={userInfo.email}
                    onChangeText={value => setEmail(value)}/>
            </View>
            <View style={styles.inputView}>
                <TextInput
                    secureTextEntry
                    style={styles.inputText}
                    placeholder="Password..."
                    placeholderTextColor="#003f5c"
                    onChangeText={value => setPassword(value)}/>
            </View>
            <TouchableOpacity onPress={handleSubmit} style={styles.loginBtn}>
                <Text style={styles.loginText}>Modifier</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={logout}>
                <Text style={styles.loginText}>Se deconnecter</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        //  backgroundColor: '#fff',
        flex: 0.6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
    inputView: {
        width: "80%",
        backgroundColor: "#465881",
        borderRadius: 25,
        height: 50,
        marginBottom: 20,
        justifyContent: "center",
        padding: 20
    },
    inputText: {
        height: 50,
        color: "white"
    },
    forgot: {
        color: "white",
        fontSize: 11
    },
    loginBtn: {
        width: "80%",
        backgroundColor: "#0090ff",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 40,
        marginBottom: 10
    },
    welcome: {
        fontWeight: 'bold',
        alignSelf: 'flex-start',
        marginLeft: 50,
    },
    info: {
        alignSelf: 'flex-start',
        marginLeft: 50,
        marginBottom: 20
    }
});
