import * as React from 'react';
import {StyleSheet, TextInput, TouchableOpacity, RefreshControl, AsyncStorage} from 'react-native';
import {Text, View} from '../components/Themed';
import * as Linking from 'expo-linking';
import {
    createStackNavigator,
    StackScreenProps,
} from '@react-navigation/stack';

export default function LoginScreen({ navigation: { navigate } }) {
    const [email, setEmail] = React.useState({})
    const [password, setPassword] = React.useState({})
    const [refreshing, setRefreshing] = React.useState(false);

    const setToken = async (token) => {
        try {
            await AsyncStorage.setItem('token', token);
            navigate('Rechercher');
        } catch (error) {
            console.error(error)
            // Error saving data
        }
    };

    const handleSubmit = (e) => {
        console.log(email)
        console.log(password)

        if (email !== undefined && password !== undefined) {
            fetch('http://46.101.147.201:1337/users/login', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({email: email, password: password})
            }).then((response) => response.json())
                .then((json) => {
                    if (json.token != undefined) {
                        setToken(json.token);
                    } else {
                        console.error(json);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });


        }
    }

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Compte</Text>
            <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)"/>
            <Text style={styles.welcome}>Bonjour!</Text>
            <Text style={styles.info}>Connectez-vous pour découvrir toutes nos fonctionnalités.</Text>
            <View style={styles.inputView}>
                <TextInput
                    style={styles.inputText}
                    placeholder="Email..."
                    placeholderTextColor="#003f5c"
                    onChangeText={value => setEmail(value)}/>
            </View>
            <View style={styles.inputView}>
                <TextInput
                    secureTextEntry
                    style={styles.inputText}
                    placeholder="Password..."
                    placeholderTextColor="#003f5c"
                    onChangeText={value => setPassword(value)}/>
            </View>
            <TouchableOpacity>
                <Text style={styles.forgot}>Forgot Password?</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={handleSubmit} style={styles.loginBtn}>
                <Text style={styles.loginText}>LOGIN</Text>
            </TouchableOpacity>
            <TouchableOpacity>
                <Text style={styles.loginText}>Signup</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        //  backgroundColor: '#fff',
        flex: 0.6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
    inputView: {
        width: "80%",
        backgroundColor: "#465881",
        borderRadius: 25,
        height: 50,
        marginBottom: 20,
        justifyContent: "center",
        padding: 20
    },
    inputText: {
        height: 50,
        color: "white"
    },
    forgot: {
        color: "white",
        fontSize: 11
    },
    loginBtn: {
        width: "80%",
        backgroundColor: "#0090ff",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 40,
        marginBottom: 10
    },
    welcome: {
        fontWeight: 'bold',
        alignSelf: 'flex-start',
        marginLeft: 50,
    },
    info: {
        alignSelf: 'flex-start',
        marginLeft: 50,
        marginBottom: 20
    }
});
