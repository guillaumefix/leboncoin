import * as React from 'react';
import {StyleSheet, TextInput} from 'react-native';
import AdsList from '../components/AdsList';
import {Text, View} from '../components/Themed';

export default function SearchScreen() {
    const [search, setSearch] = React.useState({})
    return (
        <View>
            <View style={styles.container}>
                <View style={styles.inputView}>
                    <TextInput
                        style={styles.inputText}
                        placeholder="Rechercher sur leboncoin"
                        placeholderTextColor="#003f5c"
                        onChangeText={value => setSearch(value)}/>
                </View>
                <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)"/>
            </View>
            <AdsList path="/screens/SearchScreen.tsx"/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        //backgroundColor: '#fff',
     //   flex: 0.6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    },
    inputView: {
        width: "80%",
        backgroundColor: "#ffffff",
        borderRadius: 5,
        height: 50,
        marginBottom: 20,
        justifyContent: "center",
        padding: 20
    },
    inputText: {
        height: 50,
        color: "white"
    },
});
